#___________________________________________________________libraries____________________
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer
from gensim.corpora import Dictionary
from gensim.models import TfidfModel
from nltk.stem import PorterStemmer
from collections import defaultdict
from nltk.corpus import stopwords
from num2words import num2words
from gensim import similarities
import pandas as pd
import numpy as np
import re

#___________________________________________________________stopwords____________________

stopwords = set(stopwords.words('english'))

#___________________________________________________________Processing____________________

def removeSymbols(word):
    symbols = "!\"'#$%&()*+-./,:;<=>?@[\]^_`{|}~\n\x1a"
    for i in symbols:
        word = np.char.replace(word, i, ' ')
    return str(word)

def convert_numbers(word):
    if re.search(r'\d',word):
        return num2words(re.search(r'\d+',word).group())
    else:
        return word
    
def remove_single_characters(word):
    new_word =' '
    for term in word:
        if len(term) > 1 and term not in stopwords:
            new_word = new_word+' '+term
    return new_word

def stemming_lemmatisation(word):
    #porter = PorterStemmer()
    wordnet_lemmatizer = WordNetLemmatizer()
    #word = porter.stem(word)
    word_lemm = wordnet_lemmatizer.lemmatize(word, pos="v")
    return word_lemm
    
def processing(text):
    words =''
    text = text.lower() # make lower for all text
    for word in text.split():
        if word not in stopwords:
            #remove symboles
            word = removeSymbols(word)
            #remove single characters for example she'll => she , ll  
            word = remove_single_characters(word.split())
            
            word = stemming_lemmatisation(word)
            #convert number to word
            word = convert_numbers(word)
            words = words+' '+word
          
    #print(" ".join(words.split()))
    return " ".join(words.split()) # remove multiple spaces

#___________________________________________________________dataset____________________
            
def takeUrlTitles(folder):
    titles = []
    urls = [] 
    
    file = open(folder+'/index.html','r')
    text = file.read().strip()
    file.close()
        
    #extract filename and filetitle
    file_name = re.findall('<TD ALIGN=TOP><A HREF=".*">',text)
    file_title = re.findall('<BR><TD>(.*)\n',text)

    #test length
    if len(file_name) !=0 and len(file_title) !=0 :
        for i in range(len(file_name)):

            titles.append(file_title[i]) # take title of each story
            urls.append(folder+'/'+re.sub('["\[\]\']','',str(re.findall('".*"',file_name[i])))) # take url of each story
            #print(folder+'/'+re.sub('["\[\]\']','',str(re.findall('".*"',file_name[j]))))
    
    return [urls,titles]

def collectUrlsTitles(folder):
    dataset1 = takeUrlTitles(folder) # dataset of titles and url of folder stories
    dataset2 = takeUrlTitles(folder+'/SRE') # dataset of titles and url of folder SRE
    urls = np.append(dataset1[0],dataset2[0]) # gather urls
    titles = np.append(dataset1[1],dataset2[1]) # gather titles
    return [urls,titles]
        

def collectTitlesBodies(folder):
    titlesT = []
    bodies = []
    dataset = collectUrlsTitles(folder) 
    urls = dataset[0] # urls
    titles = dataset[1] # titles
    
    urls = set(urls) # save one of each doubles
    titles = set(titles) #same thing
    
    # gather bodies
    for url,title in zip(urls,titles):
        try :
            file = open(url, encoding='utf-8')
            text = file.read()
            file.close()
            #make a processing for each body and title
            bodies.append(processing(text))
            titlesT.append(processing(title))
        except:
            continue
    print(len(titlesT),' ',len(bodies))
    r= pd.DataFrame({'title':titlesT,'body':bodies})
    r.to_csv("resultat.csv",index=False)

#___________________________________________________________TF_IDF and simolarity_______________

def remove_word_once(bodies):
    
    frequency = defaultdict(int)
    
    for body in bodies:
        for token in body:
            frequency[token] += 1

    bodies = [[token for token in body if frequency[token] > 1] for body in bodies]
    return bodies
        
def TF_IDF(titles,bodies):
    # remove words that appear only once
    #bodies1 = remove_word_once(bodies)
    
    
    #tokenized_bodies = [[line.split() for line in body] for body in bodies]
    # remove [] from bodies
    tokenized_bodies = []
    
    string =[]
    for body in bodies:
        for line in body:
            string = []
            for word in line.split():
                string.append(word.strip())
        tokenized_bodies.append(string)
    tokenized_titles = [title.split() for title in titles]
    
    #we need to create a corpus of bodies and convert corpus to BoW format
    dictionaryB = Dictionary(tokenized_bodies)
    
    corpus_body = [dictionaryB.doc2bow(body) for body in tokenized_bodies]
    # fit model
    model_body = TfidfModel(corpus_body, id2word=dictionaryB)  
    
    #we need to create a corpus of titles and convert corpus to BoW format
    dictionaryT = Dictionary(tokenized_titles)
    
    corpus_title = [dictionaryT.doc2bow(title) for title in tokenized_titles]
    # fit model
    model_title = TfidfModel(corpus_title, id2word=dictionaryT)
       
    #create a query
    #query = "Without the drive of Rebeccah's instence , Kate lost her momentum She stood next a slatted oak bench , canisters still clutched,surveying"
    query = "alternate side street screenplay jeff massie"
    #do the processings
    new_query = processing(query)
    
    #dictionary of titles
    vec_bow_queryT = dictionaryT.doc2bow(new_query.split())
    #dictionary of bodies
    vec_bow_queryB = dictionaryB.doc2bow(new_query.split())
    
    # convert the query to tf_idf space
    vec_tfidf_title = model_title[vec_bow_queryT]
    vec_tfidf_body = model_body[vec_bow_queryB]
    
    # transform corpus to tf_idf space and index it
    indexT = similarities.MatrixSimilarity(model_title[corpus_title]) 
    # perform a similarity query against the corpus
    simsT = indexT[vec_tfidf_title]  
#    # print (document_number, document_similarity) 2-tuples
#    print(list(enumerate(sims)))  

    simsT = sorted(enumerate(simsT), key=lambda item: -item[1])
    for i, s in enumerate(simsT):
        if i <=10:
            print('\n',i,s, '   :', titles[i])
    
    print('_____________________________________________________________________________')
    # transform corpus to tf_idf space and index it
    indexB = similarities.MatrixSimilarity(model_body[corpus_body]) 
    # perform a similarity query against the corpus
    simsB = indexB[vec_tfidf_body]  
#    # print (document_number, document_similarity) 2-tuples
#    print(list(enumerate(sims)))  

    simsB = sorted(enumerate(simsB), key=lambda item: -item[1])
    for i, s in enumerate(simsB):
        if i <=10:
            print(i,s, bodies[i])

#___________________________________________________________Word2Vec and simolarity__________
            
def create_dict(titles,bodies):
    #create dictionnary of titles with index and the same bodies
    dict_titles = {}
    for i in range(len(titles)):
        dict_titles[i] = titles[i]
        
    dict_bodies = {}
    for i in range(len(bodies)):
        dict_bodies[i] = bodies[i]
        
    return [dict_titles,dict_bodies]

def create_model_doc2vec(titles,bodies):

    # tagged data
    tagged_titles = [TaggedDocument(words=word_tokenize(_d.lower()), tags=[str(i)]) for i, _d in enumerate(titles)]
    tagged_bodies = [TaggedDocument(words=word_tokenize(_d[0].lower()), tags=[str(i)]) for i, _d in enumerate(bodies)]
    
    # create two models 
    model1 = Doc2Vec( vector_size=20, window=2, min_count=1, workers=4)
    model2 = Doc2Vec( vector_size=20, window=2, min_count=1, workers=4)
    
    # 
    model1.build_vocab(tagged_titles)
    model2.build_vocab(tagged_bodies)
    
    #start training our model
    for epoch in range(100):
        model1.train(tagged_titles,total_examples=model1.corpus_count,epochs=model1.iter)
        # decrease the learning rate
        model1.alpha -= 0.0002
        # fix the learning rate, no decay
        model1.min_alpha = model1.alpha
    
        #start training our model
    for epoch in range(100):
        model2.train(tagged_titles,total_examples=model2.corpus_count,epochs=model2.iter)
        # decrease the learning rate
        model2.alpha -= 0.0002
        # fix the learning rate, no decay
        model2.min_alpha = model2.alpha
    
    model1.save("doc2vec1.model")
    model2.save("doc2vec2.model")
    print("Models Saved")

def Word_tow_Vec(dict_titles,dict_bodies):
    #load models
    model1 = Doc2Vec.load("doc2vec1.model")
    model2 = Doc2Vec.load("doc2vec2.model")
    
    # the query
    query = "Without the drive of Rebeccah's instence , Kate lost her momentum She stood next a slatted oak bench , canisters still clutched,surveying"
    
    #to find the vector of a document which is not in training data
    test_data = word_tokenize(query.lower())
    
    vec_title = model1.infer_vector(test_data)
    vec_body = model2.infer_vector(test_data)
#    print("V1_infer", vec_title)

    # to find most similar title and body with query 
    similar_title = model1.docvecs.most_similar([vec_title])
    similar_body = model2.docvecs.most_similar([vec_body])
    
    # display similars titles and bodies with query
    print('__________________________ similar titles with their bodies ______________________________')
    for index_doc in similar_title:
        # get index of similar doecument with idex_doc[0] from similar_doc
        print(dict_titles[int(index_doc[0])],'\n',dict_bodies[int(index_doc[0])][0])
    print('___________________________________________________________________________')
    
    print('__________________________ similar body with their titles ______________________________')
    for index_doc in similar_body:
        # get index of similar doecument with idex_doc[0] from similar_doc
        print(dict_titles[int(index_doc[0])],'\n',dict_bodies[int(index_doc[0])][0])
    print('___________________________________________________________________________')
    
    
    # to find vector of doc in training data using tags or in other words, printing the vector of document at index 1 in training data
    #print('documents are :',model1.docvecs['1'])
    
                
#___________________________________________________________main____________________  
    
if __name__=='__main__':
    #collectTitlesBodies('stories')
    dataset = pd.read_csv('resultat.csv', sep = ',').to_numpy()
    titles = dataset[:,0] 
    bodies = dataset[:,1:]
    
    # TF_IDF
    TF_IDF(titles,bodies)
    
    # doc2vec
#    create_model_doc2vec(titles,bodies)
#    [dict_titles,dict_bodies] = create_dict(titles,bodies)
#    Word_tow_Vec(dict_titles,dict_bodies)
    